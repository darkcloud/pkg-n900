
# This is my README

These files are just me mucking about with my N900.  I've got Debian wheezy armhf, Pali's 3.8 kernel and `xserver-xorg-video-fbdev`.

bar  batmon  battery  hardware  lib  wm

The `battery` folder is a battery charger in Ruby ( not required for any kernel such as the 3.8 that charges the battery for you).

`hardware` is a D-Bus service that provides access to hardware such as the battery status and backlight.

`wm` is a really really simple window manager.

`bar` is a bar across the top that includes battery and clock applets.

`lib/XR` has ( very incomplete) Ruby bindings for Xlib.

`lib/SDLR` has ( very incomplete) Ruby bindings for SDL.


### Dependencies

 + `build-essential`
 + `dbus-x11`
 + `fonts-droid`
 + `ruby-dbus`
 + `libsdl1.2-dev`
 + `libsdl-ttf2.0-dev`
 + `libsdl-image1.2-dev`

