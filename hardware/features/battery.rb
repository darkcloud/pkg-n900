
class Battery

  def initialize
    @capacity = rd("charge_full") / 1000 # file is in micro Ah
  end

  def state
    { "time_to_empty" =>   time_to_empty,   # minutes
      "state_of_charge" => state_of_charge, # %
    }
  end


  def extended_state
    { "time_to_empty" =>    time_to_empty,
      "state_of_charge" =>  state_of_charge,
      "capacity" =>         rd("charge_full") / 1000, # provides mAh
      "rate" =>             rd("current_now") / 1000, # provides mA
      "voltage" =>          rd("voltage_now") / 1000, # provides mV
      "temperature" =>      rd("temp") / 10,          # provides deg C
    }
  end


  def state_of_charge
    rd "capacity"
  end


  def time_to_empty
    if %w{ Charging  Full }.include? File.read("/sys/class/power_supply/bq27200-0/status").chomp
        -1
      else
        rd("time_to_empty_now") / 60
    end
  end


  def rd file_name
    File.read("/sys/class/power_supply/bq27200-0/"+file_name).to_i
  end

end

