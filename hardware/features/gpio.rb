
class GPIO

  class Switch

    def initialize name
      @name = name
      @file = File.open "/sys/devices/platform/gpio-switch/"+name+"/state"
      @previous_state  = @file.read.chomp
    end

    def poll
      @file.rewind
      current_state = @file.read
      current_state.chomp!
      if current_state != @previous_state
        DbusService.switch_toggled @name, current_state
        @previous_state = current_state
      end
    end
  end

  def initialize
    @switches = %w{ kb_lock  slide  cam_focus  cam_launch  proximity  headphone }.map {|name| Switch.new  name }
  end

  def poll
    @switches.each {|s| s.poll }
  end

end

