
def Backlight home
  Backlight.new  home
end


class Backlight

  #  @param  home  The path to the device directory such as
  #                "/sys/class/backlight/acx565akm"
  #
  def initialize home
    @home = home
    @max_brightness = File.read(File.join(@home,"max_brightness")).to_i
    @brightness_file = File.join @home, "brightness"
  end

  def state
    bn = File.read(@brightness_file).to_i
    { "max_brightness" => @max_brightness,
      "brightness" => bn,
    }
  end

  def set_level new_level
    raise "Bad level: #{level}" unless (0..@max_brightness).include? new_level
    File.write  new_level, @brightness_file
  end

end

