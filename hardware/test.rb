#!/usr/bin/env ruby

$LOAD_PATH << "../lib"

require "hardware"
require "yaml"


case ARGV.first
when "btrs"
  puts Hardware.BatteryState.to_yaml
when "ebtrs"
  puts Hardware.ExtendedBatteryState.to_yaml
when "bls"
  puts Hardware.BacklightState.to_yaml
when "sbl"
  Hardware.SetBacklightLevel ARGV[ 1].to_i
when "kbbls"
  puts Hardware.KeyboardBacklightState(ARGV[1].to_i).to_yaml
when "skbbl"
  Hardware.SetKeyboardBacklightLevel *(1..2).map{|i| ARGV[ i].to_i }
when "skbbls"
  Hardware.SetKeyboardBacklightLevels ARGV[ 1].to_i
when "listen"
  Thread.new do
    Hardware.on_signal "SwitchToggled" do |switch, state|
      p [ switch, state]
    end
    Hardware.run
  end
  # Do whatever on the main thread as long as it sleeps periodically
  while true
    sleep 0.1
  end
end

