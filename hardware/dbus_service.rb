
require "dbus"
require "thread"

# An exception should terminate ALL threads:
Thread.abort_on_exception = true


# This object is the service provided to other processes via D-Bus.
#
class DbusService < DBus::Object

  def initialize hardware
    super "/Hardware"
    @hardware = hardware
  end


  # http://dbus.freedesktop.org/doc/dbus-specification.html
  dbus_interface "ro.dist.phone.HardwareAPI" do

    dbus_method :BatteryState, "out a{si}" do
      [@hardware.battery.state]
    end

    dbus_method :ExtendedBatteryState, "out a{si}" do
      [@hardware.battery.extended_state]
    end

    dbus_method :BacklightState, "out a{sy}" do
      [@hardware.display_backlight.state]
    end

    dbus_method :SetBacklightLevel, "in y" do |level|
      @hardware.display_backlight.set_level  level
    end

    dbus_method :KeyboardBacklightState, "in y, out a{sy}" do |region|
      [@hardware.keyboard_backlights[region].state]
    end

    dbus_method :SetKeyboardBacklightLevel, "in y, in y" do |region, level|
      raise "Bad region: #{region}" unless (0..5).include? region
      @hardware.keyboard_backlights[region].set_level  level
    end

    dbus_method :SetKeyboardBacklightLevels, "in y" do |level|
      (0..5).each {|region| @hardware.keyboard_backlights[region].set_level  level }
    end

    # This is to notify when a switch such as the power button is toggled
    dbus_signal :SwitchToggled, "switch:s, state:s"
  end


  class << self

    def start hardware

      Thread.new do
        puts "D-Bus thread starting"

        bus = DBus::SystemBus.instance
        service = bus.request_service "ro.dist.phone.Hardware"
        @exported_object = self.new  hardware
        service.export @exported_object

        thread = DBus::Main.new
        thread << bus
        thread.run

        puts "D-Bus thread stopped"
      end
    end

    def switch_toggled name, state
      @exported_object.SwitchToggled  name, state
    end

  end # of class methods

end

