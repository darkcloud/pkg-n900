
require "applet"


class ClockApplet < Applet

  def initialize
    @when_last_updated = 0
    @time_place = Vector::ORIGIN
    @date_place = Vector 0, 32
  end

  def preferred_width
    128
  end

  def update
    now = Time.now
    min = now.to_i / 60
    if @when_last_updated < min
      render  now
      @when_last_updated = min
    end
  end

  def render  time
    formatted_time = time.strftime "%I:%M %p"
    formatted_time = formatted_time[1..-1] if formatted_time.start_with? "0"
    write  formatted_time, @time_place
    date = time.strftime "%a %b %e"
    date.sub "  ", " " # Remove the leading zero from "%e"
    write  date, @date_place
  end

  def tapped where
    puts "tapped at "+where.inspect
  end

end

