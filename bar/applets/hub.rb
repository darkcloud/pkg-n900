
require "applet"


class HubApplet < Applet

  def initialize
    @graphics = Surface.load "applets/hub.png"
    @icon_size = Vector @graphics.width, @graphics.height
    @icon_topleft = Vector( 32 - @graphics.width / 2, 32 - @graphics.height / 2)
  end

  def preferred_width
    -64
  end

  def update
    if not @rendered
      render
      @rendered = true
    end
  end

  def render
    blit @graphics, Rect(Vector::ORIGIN,@icon_size), @icon_topleft
  end

  def tapped where
    launch "/home/neil/pkg-n900/hub/hub"
  end

end

