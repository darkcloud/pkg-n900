
require "applet"
require "hardware"


class BatteryApplet < Applet

  def initialize
    @when_last_updated = Time.now - 3
    @graphics = Surface.load "applets/battery.png"
  end

  def preferred_width
    64
  end

  def update
    # The battery status should only be queried once every 2 seconds:
    if @when_last_updated + 2 < Time.now
      previous_status = @status
      @status = Hardware.BatteryState
      if @status != previous_status
        render
      end
      @when_last_updated = Time.now
    end
  end

  def render
    if -1 == @status["time_to_empty"]
      charge = @status["state_of_charge"]
      frame = ( charge < 100) ? ( charge / 25) : 3
      icon_top_left = Vector 64*frame, 0
      icon_size = Vector 64, 64
      blit @graphics, Rect(icon_top_left,icon_size), Vector::ORIGIN
    else
      hours, minutes = @status["time_to_empty"].divmod 60
      status = "#{hours}h%02i"% minutes
      write  status, Vector::ORIGIN
    end
  end

  def tapped where
    launch "../batmon/batmon"
  end

end

