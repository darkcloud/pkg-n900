
class Applet

  attr_writer :bar
  attr_reader :area

  def area= on_screen
    @area = on_screen
    @top_left = Vector on_screen.x, on_screen.y
  end

  # For applets that don't need to update, they also don't need to provide an
  # empty update method:
  def update
  end

  #  @param  coords  Vector
  #
  def write text, coords
    @bar.font.write  text, @top_left+coords
  end

  def blit surf, from_area, to
    surf.blit  from_area, @top_left+to
  end

  def launch command
    (pid = fork) ? Process.detach( pid) : exec( command)
  end

end

