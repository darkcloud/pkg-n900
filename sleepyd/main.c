
#include <unistd.h> // for usleep()
#include <stdio.h> // for perror()
#include <stdlib.h> // for exit()
#include <stdint.h> // for uint8_t
#include <fcntl.h> // for open()
#include <time.h> // for time() and difftime()
#include <sys/stat.h> // for stat()
#include <string.h> // for strlen()
#include <stdbool.h>


#define  TIME_TO_DIM     60  // The number of seconds of inactivity before the display is dimmed
#define  TIME_TO_BLANK  120  // Seconds to blank ( since activity, rather than since dimmed)
#define  DIM_TO_LEVEL   "50"  // The bright level to set when dimming the display
#define  ACTIVE_LEVEL  "200"  // The bright level to set when activating the display


typedef enum
{
  DISPLAY_ACTIVE,
  DISPLAY_DIMMED,
  DISPLAY_BLANKED,
}
DisplayState;


DisplayState  display_state;


void static fail( char *message)
{
  perror( message);
  exit( 1);
}


void static write_to_file( char *data, char *path_to_file)
{
  int  f = open( path_to_file, O_WRONLY);
    if ( -1 == f)  fail( path_to_file);
  ssize_t  bytes_written = write( f, data, strlen(data));
    if ( -1 == bytes_written)  fail( path_to_file);
  close( f);
}


void static dim_display()
{
  write_to_file( DIM_TO_LEVEL, "/sys/class/backlight/acx565akm/brightness");
  display_state = DISPLAY_DIMMED;
}


void static blank_display()
{
  write_to_file("1", "/sys/devices/platform/omapfb/graphics/fb0/blank");
  display_state = DISPLAY_BLANKED;
}


void static activate_display()
{
  write_to_file( ACTIVE_LEVEL, "/sys/class/backlight/acx565akm/brightness");
  write_to_file("0", "/sys/devices/platform/omapfb/graphics/fb0/blank");
  display_state = DISPLAY_ACTIVE;
}


void check_for_activity()
{
  struct stat  entry;
  char *path_to_file = "/run/.tickle-sleepyd";
  int  outcome = stat( path_to_file, &entry);
    if ( -1 == outcome)  fail( path_to_file);

  time_t  now = time( NULL);
  double  seconds_since_activity = difftime( now, entry.st_mtime);

  if ( 2 < seconds_since_activity)
  {
    switch( display_state)
    {
    case DISPLAY_ACTIVE:
      if ( TIME_TO_DIM < seconds_since_activity)  dim_display();
      break;

    case DISPLAY_DIMMED:
      if ( TIME_TO_BLANK < seconds_since_activity)  blank_display();
      break;

    case DISPLAY_BLANKED:
      break;
    }
  }
  else {
    if ( display_state != DISPLAY_ACTIVE)  activate_display();
  }
}


int main( int argc, char *argv[])
{
  display_state = DISPLAY_ACTIVE;

  while( true)
  {
    check_for_activity();
    usleep( 100*1000);
  }
  return 0;
}

