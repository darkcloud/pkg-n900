#!/usr/bin/env ruby
#

require "chips"


@battery = ChargeMeter.new


puts "Status:"
status = @battery.status
status.keys.sort {|a,b| a.to_s <=> b.to_s}.each do |k|
  puts "  #{k}:#{" "*(6 - k.to_s.length)} #{status[k]}"
end
puts "Measured capacity:      #{@battery.measured_capacity}mAh"
puts "Highest discharge rate: #{@battery.highest_discharge_rate}mA"
puts "Cycles since learning:  #{@battery.cycles_since_learning}"

