#!/usr/bin/env ruby
#
# On I2C bus 2, there is a BQ24150A at address 0x6b and a BQ27200 at 0x55.  You
# pretty much *need* the datasheets on hand to understand this stuff.
#
# There is a SENSE resistor between the battery and the load ( the rest of the
# phone).  By measuring the voltage drop across the SENSE resistor, the meter
# chip can measure the current going in to or being drawn from the battery.
#
#  + TTE: predicted Time To Empty
#  + NAC: Nominal Available Capacity: an indication of how much charge is left
#         in the battery
#  + RSOC: Relative State Of Charge (%) = 100 * NAC/LMD
#  + LMD: Last Measured Discharge is a measure of the battery capacity.  The
#         LMD register is updated when the battery voltage drops to EDV1
#  + Learning cycle: A disharge from full to a low enough voltage that the
#                    meter chip will take a measurement of the capacity of the
#                    battery ( which may be lower than the design or original
#                    capacity due to wear)
#

# The value of the SENSE resistor for the charge meter:
R_SENSE = 21 # mΩ

# The value of the SENSE resistor for the charger:
CR_SENSE = 68 # mΩ


require "yaml"


class Array

  # Provides the first element of the array for which the block returns true
  def first_where
    self.each do |element|
      if yield( element)
        return element
      end
    end
    nil
  end

end


module InitializedByHash

  def initialize params
    params.each_pair do |param, value|
      instance_variable_set "@#{param}", value
    end
  end
end


# Represents a chip on an I2C bus.
#
class Chip

  include InitializedByHash

  class << self

    # Defines methods to access a register on this chip.
    #
    #  @param  name        The name of the register and thus the name of the
    #                      method used to access its current value
    #  @param  width       :b for an 8-bit wide register or :w for a 16-bit wide register
    #  @param  address     The ( base) address used to access the register
    #  @param  structures  See #pack for a description of this parameter
    #
    def register name, width, address, structures = []
      hex_digits = ( 2 if :b == width) || 4
      define_method  name do
        output = wait_for "i2cget -y #{@bus} 0x%02x 0x%02x #{width}"% [@device, address]
        if output.match Regexp.new('^0x([0-9a-f]{%i})$'% hex_digits)
          $1.to_i 16
        else
          unexpected  output
        end
      end
      define_method "#{name}=" do |new_value|
        if new_value.is_a? Hash
          new_value, mask = pack  structures, new_value
        else
          mask = nil
        end
        mask_args = " -m 0x%02x"% mask if mask
        wait_for "i2cset -y#{mask_args} #{@bus} 0x%02x 0x%02x 0x%02x"% [@device, address, new_value]
      end
    end

  end # of class methods


  def unexpected situation
    raise StandardError.new "Unexpected: #{situation}"
  end


  def wait_for command, debug = false
    if debug
      puts command
    else
      IO.popen( command) {|stream| stream.read }
    end
  end


  # Packs a map-style value in to a single byte ready for writing to a register.
  #
  #  @param  structures  A list of "structures".  Each item is a list
  #                      representing one structure ( a field packed within a
  #                      register).  The items on the sub-lists are:
  #                       [ name, bits, meanings]
  #                       + Name is the name of the structure such as :CHGS and
  #                         will ideally relate to the datasheet
  #                       + Bits is either an integer indicating the bit
  #                         position if the field is a single bit, or a Range
  #                         such as 4..5 if the field spans many bits
  #                       + Meanings is a list of the meanings of each possible
  #                         value within the structure, so if the structure is
  #                         3 bits wide then there should be 8 meanings
  #  @param  value       The values that should packed in to the register
  #
  #  @return  A 2-tuple of [ packed_value, mask] where packed_value is the byte
  #           to write to the register and mask is the mask required to write
  #           to the relevant bits.  If "value" does not include values for all
  #           structures then the remaining structures will be masked off so
  #           that they are not set to zero
  #
  # Example:
  #
  # > CTRL = [
  # >   [:TMR_RST, 7, ["No effect", "Reset the safety timer"]],
  # >   [:EN_STAT, 6, ["Disable STAT pin function", "Enable STAT pin function"]],
  # > ]
  # > packed, mask = pack  CTRL, { :TMR_RST => "Reset the safety timer",
  #                                :EN_STAT => "Disable STAT pin function" }
  # => [ 0x80, 0xc0]
  #
  def pack structures, values
    check_type  structures, Array
    # Automatically work out the mask based on keys absent from "values"
    mask = 0x00
    packed_values = values.reduce 0x00 do |packed_values, entry|
      name, value = entry
      name, bits, meanings = structures.first_where {|st| name == st[0] }
      # "bits" can either be an integer such as 3, which woudl indicate a
      # single bit at position 3 or a Range object such as 2..3, which would
      # indicate 2 bits from position 2
      position, width = position_and_width_of  bits
      check_equal  (1 << width), meanings.count
      check_within  meanings, value
      encoded_meaning = meanings.index  value
      mask |= ( (1 << width) - 1) << position
      packed_values | ( encoded_meaning << position)
    end
    mask = nil if 0xff == mask
    [ packed_values, mask]
  end

  # Unpacks values from a packed byte.
  #
  #  @param  packed_values  A list of fields, each a list with elements:
  #  @param  structures     See #pack for a description of this parameter
  #
  def unpack packed_values, structures
    check_type  structures, Array
    structures.reduce( {} ) do |values, structure|
      name, bits, meanings = structure
      position, width = position_and_width_of  bits
      values[ name] = meanings[ (packed_values >> position) & ((1 << width) - 1)]
      values
    end
  end

  # A helper for #pack and #unpack.
  #
  def position_and_width_of bits
    case bits
      when Fixnum
        [ bits, 1]
      when Range
        width = bits.max - bits.min + 1
        [ bits.min, width]
    end
  end

  def check_type object, type
    unless object.is_a? type
      oops "Expected #{type.name} but got #{object.class.name}"
    end
  end

  def check_equal expected_value, actual_value
    if actual_value != expected_value
      oops "Expected #{expected_value.inspect} but got #{actual_value.inspect}"
    end
  end

  def check_within list, value
    unless list.include? value
      oops "Expected #{value} to be one of #{list.inspect}"
    end
  end

  def oops message
    raise StandardError.new  message
  end

end


class ChargeMeter < Chip

  FLAGS_STRUCTURES = [
    [:CHGS,  7, %w{ Discharging Charging }],
    [:NOACT, 6, [0,1]],
    [:IMIN,  5, [0,1]],
    [:CI,    4, ["Capacity is accurate", "Capacity is inaccurate"]],
    [:CALIP, 3, [0,1]],
    [:VDQ,   2, ["Not learning", "Learning"]],
    [:EDV1,  1, ["Above EDV1", "BELOW EDV1"]],
    [:EDVF,  0, ["Battery not empty", "Battery COMPLETELY empty"]],
  ]

  # Registers, their widths and the addresses at which they are available
  register :TEMP,  :w, 0x06
  register :VOLT,  :w, 0x08
  register :FLAGS, :b, 0x0A, FLAGS_STRUCTURES
  register :CACT,  :w, 0x10
  register :LMD,   :w, 0x12
  register :AI,    :w, 0x14
  register :TTE,   :w, 0x16
  register :TTF,   :w, 0x18
  register :MLI,   :w, 0x1E
  register :CYCL,  :w, 0x28
  register :CSOC,  :b, 0x2C # Like RSOC but compensated for discharge rate and temperature

  def initialize
    super :bus => 2, :device => 0x55
  end

  # Provides the measured ( as opposed to design) capacity of the battery in mAh.
  #
  def measured_capacity
    # LMD is in units of 3.57 μVh
    ( self.LMD * 3.57 / R_SENSE).round
  end

  # Provides the battery voltage.  Updated every 2.56 seconds.
  #
  def voltage
    # VOLT is the battery voltage in mV
    self.VOLT / 1000.0
  end

  # Provides the temperature of the battery.  Updated every 2.56 seconds.
  #
  def temperature
    # TEMP is in units of 0.25 Kelvin
    # 273.15 K = 0°C
    ( 0.25 * self.TEMP - 273.15).to_i
  end

  # Provides a value from 0% to 100% to indicate how full or empty the battery
  # is compared to the capacity of the last properly measured discharge ( LMD).
  #
  alias_method :state_of_charge, :CSOC

  # Provides the charge left in the battery in mA.
  #
  def charge_remaining
    # CACT is in units of 3.57 μVh
    # I = V ÷ R
    ( self.CACT * 3.57 / R_SENSE).round
  end

  # Provides the current status of the charge meter chip as a Hash.
  #
  def status
    unpack self.FLAGS, FLAGS_STRUCTURES
  end

  # Reports the average current flowing through the SENSE resistor over the
  # last 5.12 seconds.  Note that this value is positive both when the battery
  # is being charged and when it is being discharged.  Use #status to determine
  # which.
  #
  def charge_or_discharge_rate
    # The AI register at address 0x14 provides the value in multiples of 3.57 μV
    # I = V ÷ R
    ( self.AI * 3.57 / R_SENSE).round
  end

  # Provides the time in minutes until the battery is predicted to be empty at
  # the current discharge rate.
  #
  alias_method :time_to_empty, :TTE

  # Provides the time in minutes until the battery is predicted to be full at
  # the current charge rate.
  #
  alias_method :time_to_full, :TTF

  # Provides the highest rate of discharge seen since the last charge.
  #
  def highest_discharge_rate
    # MLI is in units of 3.57 μV
    ( self.MLI * 3.57 / R_SENSE).round
  end

  # Provides the number of charge & discharge cycles that have happened since
  # the last learning cycle.
  alias_method :cycles_since_learning, :CYCL

end


class Charger < Chip

  INPUT_CURRENT_LIMIT_MEANINGS = YAML.load <<'.'
    - USB host with 100-mA current limit
    - USB host with 500-mA current limit
    - USB host/charger with 800-mA current limit
    - No input current limit
.
  # Registers, their widths and the addresses at which they are available
  register :STATUS, :b, 0x00, [
    [:TMR_RST, 7, ["No effect", "Reset the safety timer"]],
    [:EN_STAT, 6, ["Disable STAT pin function", "Enable STAT pin function"]],
  ]
  register :CONTROL, :b, 0x01, [
    [:INPUT_CURRENT_LIMIT, 6..7, INPUT_CURRENT_LIMIT_MEANINGS],
    [:V_LOWV,              4..5, [ 3.4, 3.5, 3.6, 3.7 ]],
    [:TE,                     3, ["Disable charge current termination", "Enable charge current termination"]],
    [:_CE,                    2, ["Charger enabled", "Charger is disabled"]],
    [:HZ_MODE,                1, ["Not high impedance mode", "High impedance mode"]], # High Z mode is for reducing power consumption in standby
    [:OPA_MODE,               0, ["Charger mode", "Boost mode"]], # Boost mode is for boosting the battery voltage to 5V for powering USB OTG devices
  ]

  # The regulation voltage is 3.5V + 20mV * V_OUT
  register :CURRENT, :b, 0x02, [
    [:V_OUT,  2..7, (0..63).map{|step| 3.5 + 0.020*step}],
    [:OTG_PL,    1, ["Active at Low level", "Active at High level"]],
    [:OTG_EN,    0, ["Disable OTG pin", "Enable OTG Pin"]],
  ]
  # The charge current is 550mA + 6.8 * V_ICHRG / CR_SENSE
  # The termination current is 50mA + 3.4 * V_ITERM / CR_SENSE
  register :VOLTAGE, :b, 0x04, [
    [:RESET,      7, ["No effect", "Charger in reset mode"]],
    [:V_ICHRG, 4..6, (0..7).map{|step| 550 + (6.8*step/CR_SENSE*1000).round}],
    [:NA,         3, [0,1]],
    [:V_ITERM, 0..2, (0..7).map{|step| 50 + (3.4*step/CR_SENSE*1000).round}],
  ]

  def initialize
    super :bus => 2, :device => 0x6b
  end

  CHARGE_FAULT_MEANINGS = YAML.load <<'.'
    - Normal
    - VBUS OVP
    - Sleep mode
    - Poor input source or VBUS < UVLO
    - Output OVP
    - Thermal shutdown
    - Timer fault
    - No battery
.
  BOOST_FAULT_MEANINGS = YAML.load <<'.'
    - Normal
    - VBUS OVP
    - Over load
    - Battery voltage is too low
    - Battery OVP
    - Thermal shutdown
    - Timer fault
    - NA
.

  # Provides the current status of the charger as a Hash.
  #
  def status
    value = self.STATUS
    boost = (value >> 3) & 1
    unpack  value, [
      [:OTG,   7,  %w{ Low High }],
      [:STAT,  4..5, ["Ready", "Charge in progress", "Charge done", "Fault"]],
      [:BOOST, 3,  %w{ No Yes }],
      [:FAULT, 0..2, boost ? BOOST_FAULT_MEANINGS : CHARGE_FAULT_MEANINGS],
    ]
  end

  # BQ24150A has a safety timer.  If the host does not tickle the timer every
  # 32 seconds then the chip terminates the charge.
  #
  def reset_timer
    self.STATUS = {:TMR_RST => "Reset the safety timer"}
  end
end

