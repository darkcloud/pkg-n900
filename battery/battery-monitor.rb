#!/usr/bin/env ruby
#

require "chips"


@battery = ChargeMeter.new


while true do

  battery_status = @battery.status
  rate = @battery.charge_or_discharge_rate
  if "Discharging" == battery_status[:CHGS]
    rate = - rate
  end

  hours, minutes = @battery.time_to_empty.divmod 60
  time_to_empty = "#{hours}h%02i"% minutes
  %w{ CALIP  NOACT  IMIN  CHGS  CI }.each {|k| battery_status.delete k.to_sym }
  puts  "#{battery_status.inspect} #{@battery.voltage}V #{@battery.charge_remaining}mAh #{@battery.temperature}°C (#{@battery.state_of_charge}%) rate:#{rate}mA TTE:#{time_to_empty}"
  sleep  2
end

