#!/usr/bin/env ruby
#
# Battery charging daemon.
#
# Based on ShadowJK's bash script to do the same thing.
#
#  + The battery voltage should *never* exceed 4.2V.  Bomb out with a warning
#    if it does
#

REGV = 4.2


require "yaml"
require "chips"


@battery = ChargeMeter.new
@charger = Charger.new


def configure
  puts "Charger: #{ File.read '/sys/devices/platform/musb_hdrc/charger'}"

  puts "Pre-Config Status:"
  puts @charger.status.inspect
  puts @charger.status.inspect

  # Disable charger for configuration:
  @charger.CONTROL = { :INPUT_CURRENT_LIMIT => "No input current limit",
                       :V_LOWV =>               3.4,
                       :TE =>                  "Enable charge current termination",
                       :_CE =>                 "Charger is disabled",
                       :HZ_MODE =>             "Not high impedance mode",
                       :OPA_MODE =>            "Charger mode",
                     }
  @charger.VOLTAGE = { :RESET =>  "No effect",
                       :V_ICHRG => 950,
                       :NA =>      0,
                       :V_ITERM => 150,
                     }
  @charger.CURRENT = { :V_OUT =>   REGV,
                       #:OTG_PL => "Active at Low level",
                       #:OTG_EN => "Disable OTG pin",
                     }
  @charger.CONTROL = { :INPUT_CURRENT_LIMIT => "USB host with 100-mA current limit",
                       :V_LOWV =>               3.4,
                       :TE =>                  "Enable charge current termination",
                       :_CE =>                 "Charger enabled",
                       :HZ_MODE =>             "Not high impedance mode",
                       :OPA_MODE =>            "Charger mode",
                     }
  @charger.STATUS = { :TMR_RST => "No effect",
                      :EN_STAT => "Disable STAT pin function",
                    }
  soft_start_steps = YAML.load <<'.'
    - USB host with 100-mA current limit
    - USB host with 500-mA current limit
    - USB host/charger with 800-mA current limit
    - No input current limit
.
  soft_start_steps.each do |current_limit|
    @charger.CONTROL = { :INPUT_CURRENT_LIMIT => current_limit,
                         :TE => "Enable charge current termination",
                       }
    sleep 1
    puts @charger.status.inspect
  end
  @charger.reset_timer
  # Reading this file triggers side-effects such as reducing power use by 80mA
  # when a wall charger is being used
  File.read "/sys/devices/platform/musb_hdrc/charger"
end

configure

full = false
mode = :standby
sleep_time = 15

while true do
  charger_status = @charger.status
  @charger.reset_timer
  battery_voltage = @battery.voltage
  if 4.2 < battery_voltage
    $stderr.puts "***CRITICAL*** Battery voltage #{battery_voltage} exceeds 4.2V!!!"
    exit 2
  end
  case mode
    when :standby
      if "Charge in progress" == charger_status[:STAT]
        mode = :charging
        sleep_time = 5
        puts "Charging"
        File.read "/sys/devices/platform/musb_hdrc/charger"
      end
    when :charging
      if "Ready" == charger_status[:STAT]
        mode = :standby
        sleep_time = 15
        puts "Charging complete"
        # This will stop USB from eating power as long as you haven't plugged it into a PC
        `echo 0 > /sys/devices/platform/musb_hdrc/connect`
      end
  end
  case charger_status[:STAT]
    when "Charge done"
      unless full
        puts "Battery full"
        full = true
      end
    when "Ready"
      if full
        full = false
      end
  end
  battery_status = @battery.status
  rate = @battery.charge_or_discharge_rate
  if "Discharging" == battery_status[:CHGS]
    rate = - rate
  end

  puts  "Mode:#{mode} Status:#{charger_status.inspect} #{battery_voltage}V #{@battery.temperature}°C #{@battery.charge_remaining}mAh (#{@battery.state_of_charge}%) rate:#{rate}mA"
  sleep  sleep_time
end

