
# Battery related code

 + `chips.rb` has classes that represent the BQ27200 charge meter and BQ24150A charger
 + `battery-info.rb` provides some less dynamic information such as last measured capacity
 + `battery-monitor.rb` provides a rolling report of voltage, current, etc.
 + `battery-charger.rb` charges the battery by controlling the BQ24150A

