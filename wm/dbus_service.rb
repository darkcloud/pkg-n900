
require "dbus"
require "thread"

# An exception should terminate ALL threads:
Thread.abort_on_exception = true


# This object is the service provided to other processes via D-Bus.
#
class DbusService < DBus::Object

  def initialize window_manager
    super "/WindowManager"
    @window_manager = window_manager
  end


  dbus_interface "ro.dist.wm.WindowManagerAPI" do

    dbus_method :BarHeight, "out bar_height:i" do
      @window_manager.bar_height
    end

    dbus_method :RaiseWindow, "in window_id:s" do |window_id|
      @window_manager.raise_window  window_id.to_i
    end

    dbus_method :CloseTopmostWindow do
      @window_manager.close_topmost_window
    end

    # "aa{ss}" is D-Bus-speak for a list of maps that have strings for both keys and values
    dbus_method :Windows, "out windows:aa{ss}" do
      wd = @window_manager.windows.map do |wd|
        wm_name = wd.wm_name
        attrs = {"id" => wd.id.to_s }
        attrs["name"] = wm_name if wm_name
        attrs
      end
      # Weird that it needs wrapping in another array
      [ wd]
    end

    #def without_exception
    #  begin
    #    yield
    #  rescue StandardError => e
    #    $stderr.puts e.message
    #    $stderr.puts e.backtrace
    #  end
    #end

    # This is invoked by the Window Manager to notify the Bar that it should move
    dbus_signal :BarAreaChanged, "area:s"
  end


  class << self

    def start window_manager

      Thread.new do
        puts "D-Bus thread starting"

        bus = DBus::SessionBus.instance
        service = bus.request_service "ro.dist.wm.WindowManager"
        service.export self.new( window_manager)

        thread = DBus::Main.new
        thread << bus
        thread.run

        puts "D-Bus thread stopped"
      end
    end

  end # of class methods

end

