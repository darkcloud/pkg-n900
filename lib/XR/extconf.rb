#!/usr/bin/env ruby

require "mkmf"

dir_config("XR", "/usr/include", "")
$libs = append_library($libs, "X11")

create_makefile("XR")

