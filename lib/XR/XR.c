
#include <ruby.h>
#include <X11/Xlib.h>
#include <stdlib.h> // for getenv()


#define DEBUG
//#define  DEBUG_GC


VALUE  rb_mXR,
       rb_cVector,
       rb_cDisplay,
       rb_cScreen,
       rb_cWindow,
       rb_cEvent,
       rb_cCreateNotifyEvent,
       rb_cDestroyNotifyEvent,
       rb_cUnmapNotifyEvent,
       rb_cMapNotifyEvent,
       rb_cMapRequestEvent,
       rb_cConfigureNotifyEvent,
       rb_cConfigureRequestEvent;


Atom  WM_NAME;


#define  ARRAY_LEN( ary)  ( sizeof(ary) / sizeof(ary[0]) )


#define  CLASS( class, superclass)  rb_define_class_under( rb_mXR, class, superclass)


#define UNWRAP( source_variable, type, new_variable) \
  type  *new_variable;\
  if ( Qnil == source_variable) {\
    new_variable = NULL;\
  }\
  else {\
    Data_Get_Struct( source_variable, type, new_variable);\
  }


// if DEBUG is defined then all printfs should be ignored
#ifndef DEBUG

#define printf ignore

inline void ignore( char *format, ...)
{
}

#endif


// This is a separate log channel for debugging GC problems because GC output
// is very verbose.
inline
void log_gc( char *format, ...)
{
      #ifdef DEBUG_GC
  va_list  arg_ptr;
  va_start( arg_ptr, format);
  vprintf( format, arg_ptr);
  va_end( arg_ptr);
      #endif
}


// A helper method that only marks a referenced object if it NOT NULL
inline
void maybe_mark( VALUE object)
{
  if ( object != 0)
    rb_gc_mark( object);
}


void default_free( void *this)
{
  log_gc("free %p\n", this);
  free( this);
}


// ---------------------------------------------------------------------- Symbol

VALUE rb_sx,
      rb_sy,
      rb_swidth,
      rb_sheight,
      rb_sborder_width,
      rb_ssibling,
      rb_sstack_mode;

typedef struct
{
  char  *name;
  VALUE *symbol;
}
Symbol;

Symbol  symbol_data[] = {
  {"x",             &rb_sx },
  {"y",             &rb_sy },
  {"width",         &rb_swidth },
  {"height",        &rb_sheight },
  {"border_width",  &rb_sborder_width },
  {"sibling",       &rb_ssibling },
  {"stack_mode",    &rb_sstack_mode },
};

void Init_symbols()
{
  int  i;
  for ( i = 0;  i < ARRAY_LEN(symbol_data);  i += 1)
  {
    Symbol *init = &symbol_data[ i];
    *init->symbol = rb_str_intern( rb_str_new2(init->name) );
  }
}


// ---------------------------------------------------------------------- Vector

// FIXME: Rebase on XPoint ( short x,y)
typedef struct
{
  int  x, y;
}
Vector;


VALUE Vector_allocate( VALUE class)
{
  Vector *self = ALLOC( Vector);
  return Data_Wrap_Struct( class, NULL, default_free, self);
}


VALUE Vector_initialize( VALUE rb_self, VALUE rb_x, VALUE rb_y)
{
  UNWRAP( rb_self, Vector, self)
  self->x = FIX2INT( rb_x);
  self->y = FIX2INT( rb_y);
  return Qnil;
}


static
VALUE new_Vector( int x, int y)
{
  VALUE  rb_vector = rb_obj_alloc( rb_cVector);
  UNWRAP( rb_vector, Vector, vector)
  vector->x = x;
  vector->y = y;
  return rb_vector;
}


VALUE XR_Vector( VALUE rb_module, VALUE rb_x, VALUE rb_y)
{
  return new_Vector( FIX2INT(rb_x), FIX2INT(rb_y));
}


VALUE Vector_x( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( self->x);
}


VALUE Vector_x_equals( VALUE rb_self, VALUE rb_x)
{
  UNWRAP( rb_self, Vector, self)
  self->x = FIX2INT( rb_x);
  return Qnil;
}


VALUE Vector_y( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( self->y);
}


VALUE Vector_y_equals( VALUE rb_self, VALUE rb_y)
{
  UNWRAP( rb_self, Vector, self)
  self->y = FIX2INT( rb_y);
  return Qnil;
}


VALUE Vector_plus( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  return new_Vector( self->x+other->x, self->y+other->y);
}


VALUE Vector_minus( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  return new_Vector( self->x-other->x, self->y-other->y);
}


VALUE Vector_subtract_bang( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  self->x -= other->x;
  self->y -= other->y;
  return rb_self;
}


VALUE Vector_length_squared( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( self->x*self->x + self->y*self->y);
}


VALUE Vector_inspect( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  char  inspect[ 128];
  snprintf( inspect, sizeof(inspect), "#<XR::Vector:%p @x=%i @y=%i>", (void*)rb_self, self->x, self->y);
  return rb_str_new2( inspect);
}


void Init_Vector()
{
  rb_cVector = CLASS("Vector", rb_cObject);
  rb_define_alloc_func( rb_cVector, Vector_allocate);
  rb_define_method( rb_cVector, "initialize", Vector_initialize, 2);
  rb_define_method( rb_cVector, "x", Vector_x, 0);
  rb_define_method( rb_cVector, "x=", Vector_x_equals, 1);
  rb_define_method( rb_cVector, "y", Vector_y, 0);
  rb_define_method( rb_cVector, "y=", Vector_y_equals, 1);
  rb_define_method( rb_cVector, "+", Vector_plus, 1);
  rb_define_method( rb_cVector, "-", Vector_minus, 1);
  rb_define_method( rb_cVector, "subtract!", Vector_subtract_bang, 1);
  rb_define_method( rb_cVector, "length_squared", Vector_length_squared, 0);
  rb_define_method( rb_cVector, "inspect", Vector_inspect, 0);
}


// ---------------------------------------------------------------------- Window

typedef struct
{
  Display *display;
  Window   id;
}
WindowBox;


VALUE Window_allocate( VALUE class)
{
  WindowBox *self = ALLOC( WindowBox);
  return Data_Wrap_Struct( class, NULL, default_free, self);
}


VALUE Window_properties( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)

  VALUE  rb_properties = rb_ary_new();

  int  num_properties;
  Atom *property_names = XListProperties( self->display, self->id, &num_properties);
  if ( property_names != NULL)
  {
    int  i;
    for ( i = 0;  i < num_properties;  i += 1)
    {
      char *property_name = XGetAtomName( self->display, property_names[i]);
      rb_ary_push( rb_properties, rb_str_new2(property_name));
      XFree( property_name);
    }
    XFree( property_names);
  }

  return rb_properties;
}


VALUE _Window_property( WindowBox *window, Atom property)
{
  long  offset = 0;
  long  length = -1;
  Bool  delete = False;
  Atom  actual_type; // Unfortunately, segfaults if pass NULL
  int   actual_format;
  unsigned long  num_items;
  unsigned long  bytes_after;
  unsigned char *value;
  int  outcome = XGetWindowProperty( window->display, window->id, property, offset,
      length, delete, AnyPropertyType, &actual_type, &actual_format, &num_items,
      &bytes_after, &value);
    if ( outcome != Success) rb_raise( rb_eStandardError, "XGetWindowProperty returned %i", outcome);

  //printf("bytes_after: %lu\n", bytes_after);
  VALUE  rb_value = (value != NULL) ? rb_str_new2( (char*)value) : Qnil;
  if ( value != NULL)
    XFree( value);

  return rb_value;
}


VALUE Window_property( VALUE rb_self, VALUE rb_property_name)
{
  UNWRAP( rb_self, WindowBox, self)

  Atom  property = XInternAtom( self->display, StringValueCStr(rb_property_name), True);

  return ( property != None) ? _Window_property( self, property): Qnil;
}


VALUE Window_wm_name( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)

  return _Window_property( self, WM_NAME);
}


VALUE Window_id( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)
  return ULONG2NUM( self->id);
}


VALUE Window_children( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)

  Window       *children;
  unsigned int  child_count;
  Window        root_window, parent_window;
  Status  outcome = XQueryTree( self->display, self->id, &root_window, &parent_window, &children, &child_count);
    if ( 0 == outcome) rb_raise( rb_eStandardError, "XQueryTree returned %i", outcome);

  VALUE  rb_children = rb_ary_new2( child_count);
  int  i;
  for ( i = 0;  i < child_count;  i += 1)
  {
    VALUE  rb_child = rb_obj_alloc( rb_cWindow);
    UNWRAP( rb_child, WindowBox, child)
    child->display = self->display;
    child->id = children[ i];
    rb_ary_push( rb_children, rb_child);
  }

  if ( children != NULL);
    XFree( children);

  return rb_children;
}


VALUE Window_change_attributes( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)

  XSetWindowAttributes  attributes = { event_mask: SubstructureRedirectMask | SubstructureNotifyMask };
  int  outcome = XChangeWindowAttributes( self->display, self->id, CWEventMask, &attributes);
    if ( outcome != 1) rb_raise( rb_eStandardError, "XChangeWindowAttributes: %i", outcome);
  /*
  printf("outcome: %i\n", outcome);
  printf("BadRequest: %i\n", BadRequest);
  printf("BadAccess: %i\n", BadAccess);
  printf("BadColor: %i\n", BadColor);
  printf("BadCursor: %i\n", BadCursor);
  printf("BadMatch: %i\n", BadMatch);
  printf("BadPixmap: %i\n", BadPixmap);
  printf("BadValue: %i\n", BadValue);
  printf("BadWindow: %i\n", BadWindow);
  */
  return Qnil;
}


VALUE Window_map( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)

  int  outcome = XMapWindow( self->display, self->id);
    if ( outcome != 1) rb_raise( rb_eStandardError, "XMapWindow returned %i", outcome);

  return Qnil;
}


typedef struct
{
  VALUE     rb_key; // The key to look up in the map
  int      *box;    // The place to put the value
  unsigned  mask;   // The element to add to the mask so that the value is used
}
ChangesInit;

VALUE Window_configure( VALUE rb_self, VALUE rb_changes)
{
  UNWRAP( rb_self, WindowBox, self)

  XWindowChanges  changes;/* = {
    x: 0,
    y: 64,
    width: 800,
    height: 480-64,
    border_width: 0,
    sibling: 0,
    stack_mode: 0,
  };*/
  unsigned  mask = 0;

  // This is not much less code than the unrolled version but the unrolled version is dull
  ChangesInit inits[] = {
    { rb_sx,            &changes.x,             CWX },
    { rb_sy,            &changes.y,             CWY },
    { rb_swidth,        &changes.width,         CWWidth },
    { rb_sheight,       &changes.height,        CWHeight },
    { rb_sborder_width, &changes.border_width,  CWBorderWidth },
    { rb_sstack_mode,   &changes.stack_mode,    CWStackMode },
  };
  int  i;
  for ( i = 0;  i < ARRAY_LEN( inits);  i += 1)
  {
    ChangesInit *in = &inits[ i];
    VALUE  rb_value = rb_hash_lookup( rb_changes, in->rb_key);
    if ( rb_value != Qnil)
    {
      *in->box = FIX2INT( rb_value);
      mask |= in->mask;
    }
  }
  // The sibling member must be dealt with separately because it's not an int
  VALUE  rb_sibling = rb_hash_lookup( rb_changes, rb_ssibling);
  UNWRAP( rb_sibling, WindowBox, sibling)
  if ( sibling != NULL) {
    changes.sibling = sibling->id;
    mask |= CWSibling;
  }

  //printf("XConfigureWindow 0x%x\n", mask);
  int  outcome = XConfigureWindow( self->display, self->id, mask, &changes);
    if ( outcome != 1) rb_raise( rb_eStandardError, "XConfigureWindow returned %i", outcome);

  return Qnil;
}


VALUE Window_destroy( VALUE rb_self)
{
  UNWRAP( rb_self, WindowBox, self)

  int  outcome = XDestroyWindow( self->display, self->id);
    if ( outcome != 1) rb_raise( rb_eStandardError, "XDestroyWindow returned %i", outcome);

  return Qnil;
}


void Init_Window()
{
  rb_cWindow = CLASS("Window", rb_cObject);
  rb_define_alloc_func( rb_cWindow, Window_allocate);
  rb_define_method( rb_cWindow, "id", Window_id, 0);
  rb_define_method( rb_cWindow, "children", Window_children, 0);
  rb_define_method( rb_cWindow, "change_attributes", Window_change_attributes, 0);
  rb_define_method( rb_cWindow, "map", Window_map, 0);
  rb_define_method( rb_cWindow, "configure", Window_configure, 1);
  rb_define_method( rb_cWindow, "properties", Window_properties, 0);
  rb_define_method( rb_cWindow, "property", Window_property, 1);
  rb_define_method( rb_cWindow, "wm_name", Window_wm_name, 0);
  rb_define_method( rb_cWindow, "destroy", Window_destroy, 0);
  rb_define_const( rb_cWindow, "ABOVE", INT2FIX(Above));
}


// ----------------------------------------------------------------------- Event

// These are handled polymorphically to give some assurance that when new
// events are added, they are both initialized and handled.
typedef struct
{
  VALUE *rb_class;
  char  *name;
  int    type; // MapRequest or similar
}
EventType;

EventType  event_types[] = {
  { &rb_cCreateNotifyEvent, "CreateNotifyEvent", CreateNotify },
  { &rb_cDestroyNotifyEvent, "DestroyNotifyEvent", DestroyNotify },
  { &rb_cUnmapNotifyEvent, "UnmapNotifyEvent", UnmapNotify },
  { &rb_cMapNotifyEvent, "MapNotifyEvent", MapNotify },
  { &rb_cMapRequestEvent, "MapRequestEvent", MapRequest },
  { &rb_cConfigureNotifyEvent, "ConfigureNotifyEvent", ConfigureNotify },
  { &rb_cConfigureRequestEvent, "ConfigureRequestEvent", ConfigureRequest },
};


VALUE Event_allocate( VALUE class)
{
  XEvent *self = ALLOC( XEvent);
  return Data_Wrap_Struct( class, NULL, XFree, self);
}


VALUE Event_type( VALUE rb_self)
{
  UNWRAP( rb_self, XEvent, self)
  return INT2FIX( self->type);
}


VALUE Event_window( VALUE rb_self)
{
  UNWRAP( rb_self, XEvent, self)

  // TODO: Consider allocating the Window when the event is created instead
  VALUE  rb_window = rb_obj_alloc( rb_cWindow);
  UNWRAP( rb_window, WindowBox, window)
  window->display = self->xany.display;
  Window  w;
  switch ( self->type)
  {
    case CreateNotify:      w = self->xcreatewindow.window; break;
    case DestroyNotify:     w = self->xdestroywindow.window; break;
    case UnmapNotify:       w = self->xunmap.window; break;
    case MapNotify:         w = self->xmap.window; break;
    case MapRequest:        w = self->xmaprequest.window; break;
    case ConfigureNotify:   w = self->xconfigure.window; break;
    case ConfigureRequest:  w = self->xconfigurerequest.window; break;
    default: w = 0;
  }
  if ( w != 0)
    window->id = w;
  return rb_window;
}


VALUE Event_width( VALUE rb_self)
{
  UNWRAP( rb_self, XEvent, self)

  if ( self->type != ConfigureRequest)
    rb_raise( rb_eNoMethodError, "width");

  return INT2FIX( self->xconfigurerequest.width);
}


VALUE Event_height( VALUE rb_self)
{
  UNWRAP( rb_self, XEvent, self)

  if ( self->type != ConfigureRequest)
    rb_raise( rb_eNoMethodError, "width");

  return INT2FIX( self->xconfigurerequest.height);
}


void Init_Event()
{
  rb_cEvent = CLASS("Event", rb_cObject);
  rb_define_alloc_func( rb_cEvent, Event_allocate);
  rb_define_method( rb_cEvent, "type", Event_type, 0);
  rb_define_method( rb_cEvent, "window", Event_window, 0);
  rb_define_method( rb_cEvent, "width", Event_width, 0);
  rb_define_method( rb_cEvent, "height", Event_height, 0);

  int  i;
  for ( i = 0;  i < ARRAY_LEN(event_types);  i += 1)
  {
    EventType *et = &event_types[ i];
    *et->rb_class = CLASS( et->name, rb_cEvent);
  }
}


// ---------------------------------------------------------------------- Screen

typedef struct
{
  Display *display;
  int      number;
}
ScreenBox;


VALUE Screen_allocate( VALUE class)
{
  ScreenBox *self = ALLOC( ScreenBox);
  return Data_Wrap_Struct( class, NULL, default_free, self);
}


VALUE Screen_width( VALUE rb_self)
{
  UNWRAP( rb_self, ScreenBox, self)
  return INT2FIX( DisplayWidth( self->display, self->number) );
}


VALUE Screen_height( VALUE rb_self)
{
  UNWRAP( rb_self, ScreenBox, self)
  return INT2FIX( DisplayHeight( self->display, self->number) );
}


VALUE Screen_root_window( VALUE rb_self)
{
  UNWRAP( rb_self, ScreenBox, self)
  VALUE  rb_window = rb_obj_alloc( rb_cWindow);
  UNWRAP( rb_window, WindowBox, window)
  window->display = self->display;
  window->id = RootWindow( self->display, self->number);
  return rb_window;
}


void Init_Screen()
{
  rb_cScreen = CLASS("Screen", rb_cObject);
  rb_define_alloc_func( rb_cScreen, Screen_allocate);
  rb_define_method( rb_cScreen, "width", Screen_width, 0);
  rb_define_method( rb_cScreen, "height", Screen_height, 0);
  rb_define_method( rb_cScreen, "root_window", Screen_root_window, 0);
}


// --------------------------------------------------------------------- Display

void Display_free( void *self)
{
  printf("Closing display\n");
  XCloseDisplay( self);
}


VALUE Display_open( VALUE class, VALUE rb_display_name)
{
  char *display_name = StringValueCStr( rb_display_name);

  printf("Opening display: %s\n", display_name);

  Display *display = XOpenDisplay( display_name);
    if ( NULL == display) rb_raise( rb_eStandardError, "Unable to open display: %s", display_name);

  WM_NAME = XInternAtom( display, "WM_NAME", False);

  return Data_Wrap_Struct( rb_cDisplay, NULL, Display_free, display);
}


VALUE Display_screens( VALUE rb_self)
{
  UNWRAP( rb_self, Display, self)
  int  number_of_screens = ScreenCount( self);
  VALUE  rb_screens = rb_ary_new2( number_of_screens);
  int  s;
  for ( s = 0;  s < number_of_screens;  s += 1) {
    VALUE  rb_screen = rb_obj_alloc( rb_cScreen);
    UNWRAP( rb_screen, ScreenBox, screen)
    screen->display = self;
    screen->number = s;
    rb_ary_push( rb_screens, rb_screen);
  }
  return rb_screens;
}


VALUE Display_check_event( VALUE rb_self)
{
  UNWRAP( rb_self, Display, self)
  XEvent  ev;
  Bool  got_event = XCheckMaskEvent( self, -1, &ev);
  if ( ! got_event)
    return Qnil;

  VALUE  class = rb_cEvent;

  int  i;
  for ( i = 0;  i < ARRAY_LEN(event_types);  i += 1)
  {
    EventType *et = &event_types[ i];

    if ( ev.type == et->type)
    {
      class = *et->rb_class;
      break;
    }
  }

  VALUE  rb_event = rb_obj_alloc( class);
  UNWRAP( rb_event, XEvent, event)
  memcpy( event, &ev, sizeof(XEvent));
  return rb_event;
}


VALUE Display_flush( VALUE rb_self)
{
  UNWRAP( rb_self, Display, self)

  XFlush( self);

  return Qnil;
}


void Init_Display()
{
  rb_cDisplay = CLASS("Display", rb_cObject);
  rb_define_singleton_method( rb_cDisplay, "open", Display_open, 1);
  rb_define_method( rb_cDisplay, "screens", Display_screens, 0);
  rb_define_method( rb_cDisplay, "check_event", Display_check_event, 0);
  rb_define_method( rb_cDisplay, "flush", Display_flush, 0);
}


// ------------------------------------------------------------------------ Init

void Init_XR()
{
  rb_mXR = rb_define_module("XR");

  Init_symbols();
  Init_Vector();
  Init_Display();
  Init_Screen();
  Init_Window();
  Init_Event();
}

// -------------------------------------------------------------- 80 column rule
