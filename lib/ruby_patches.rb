
module InitializedByHash

  def initialize params = {}
    params.each_pair do |param, value|
      # Use the mutator if present
      if respond_to? "#{param}="
        send "#{param}=", value
      else # Otherwise set an instance variable
        instance_variable_set "@#{param}", value
      end
    end
    # If an after_initialize is present, it may be used to further initialize
    # or provide default values for variables
    after_initialize if respond_to? :after_initialize
  end

end


class Array

  # Provides the first element of the array for which the block returns true
  def first_where
    self.each do |element|
      if yield( element)
        return element
      end
    end
    nil
  end


  def last_where
    (self.count-1).downto( 0) do |i|
      element = self[ i]
      if yield( element)
        return element
      end
    end
    nil
  end

end


class Exception

  def info
    "%s\n\t%s" % [ message, backtrace.join("\n\t")]
  end

end


class File

  class << self

    def write data, path_to_file
      File.open  path_to_file, "w" do |file|
        file.write  data
      end
    end
  end

end

