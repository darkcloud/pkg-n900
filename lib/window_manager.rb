
require "dbus"


class WindowManager

  bus = DBus::SessionBus.instance

  service = bus.service "ro.dist.wm.WindowManager"
  @remote = service.object "/WindowManager"
  @remote.introspect
  @remote.default_iface = "ro.dist.wm.WindowManagerAPI"

  class << self
    def method_missing method_name, *params
      @remote.send( method_name, *params).first
    end
  end

end

