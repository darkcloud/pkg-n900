#!/usr/bin/env ruby

require "mkmf"

dir_config("SDLR", "/usr/include", "")
$libs = append_library($libs, "SDL")
$libs = append_library($libs, "SDL_ttf")
$libs = append_library($libs, "SDL_image")
$libs = append_library($libs, "m")

create_makefile("SDLR")

