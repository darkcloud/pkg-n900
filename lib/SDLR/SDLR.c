
#include <ruby.h>
#include <stdbool.h>
#include <math.h>
// http://www.libsdl.org/cgi/docwiki.fcg/
#include "SDL/SDL.h"
// http://www.libsdl.org/projects/docs/SDL_ttf/
#include "SDL/SDL_ttf.h"
// http://www.libsdl.org/projects/docs/SDL_image/
#include "SDL/SDL_image.h"


#define DEBUG
//#define  DEBUG_GC


typedef struct
{
  Sint16  x, y;
}
Vector;


#define  TRANSPARENT  0x00
#define  OPAQUE       0xff

typedef struct
{
  TTF_Font  *font;
  SDL_Color  background_color;
  SDL_Color  text_color;
}
FontBox;


SDL_Surface  *screen;


VALUE  rb_mSDLR,
       rb_cVector,
       rb_cRect,
       rb_cSurface,
       rb_cFont,
       rb_cEvent,
       rb_cKeyboardEvent,
       rb_cKeyPressedEvent,
       rb_cKeyReleasedEvent,
       rb_cMouseMotionEvent,
       rb_cMouseButtonEvent,
       rb_cMouseButtonPressedEvent,
       rb_cMouseButtonReleasedEvent;

VALUE  rb_sLEFT_BUTTON,
       rb_sRIGHT_BUTTON,
       rb_sMIDDLE_BUTTON;


#define  ARRAY_LEN( ary)  ( sizeof(ary) / sizeof(ary[0]) )


#define CLASS( class, superclass) rb_define_class_under( rb_mSDLR, class, superclass)


#define UNWRAP( source_variable, type, new_variable) \
  type  *new_variable;\
  if ( Qnil == source_variable) {\
    new_variable = NULL;\
  }\
  else {\
    Data_Get_Struct( source_variable, type, new_variable);\
  }


// if DEBUG is defined then all printfs should be ignored
#ifndef DEBUG

#define printf ignore

inline void ignore( char *format, ...)
{
}

#endif


// This is a separate log channel for debugging GC problems because GC output
// is very verbose.
inline
void log_gc( char *format, ...)
{
      #ifdef DEBUG_GC
  va_list  arg_ptr;
  va_start( arg_ptr, format);
  vprintf( format, arg_ptr);
  va_end( arg_ptr);
      #endif
}


// A helper method that only marks a referenced object if it NOT NULL
inline
void maybe_mark( VALUE object)
{
  if ( object != 0)
    rb_gc_mark( object);
}


void default_free( void *this)
{
  log_gc("free %p\n", this);
  free( this);
}


// ---------------------------------------------------------------------- Vector

VALUE Vector_allocate( VALUE class)
{
  Vector *self = ALLOC( Vector);
  return Data_Wrap_Struct( class, NULL, default_free, self);
}


VALUE Vector_initialize( VALUE rb_self, VALUE rb_x, VALUE rb_y)
{
  UNWRAP( rb_self, Vector, self)
  self->x = NUM2INT( rb_x);
  self->y = NUM2INT( rb_y);
  return Qnil;
}


static
VALUE new_Vector( Sint16 x, Sint16 y)
{
  VALUE  rb_vector = rb_obj_alloc( rb_cVector);
  UNWRAP( rb_vector, Vector, vector)
  vector->x = x;
  vector->y = y;
  return rb_vector;
}


VALUE SDLR_Vector( VALUE rb_module, VALUE rb_x, VALUE rb_y)
{
  return new_Vector( NUM2INT(rb_x), NUM2INT(rb_y));
}


VALUE Vector_x( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( self->x);
}


VALUE Vector_x_equals( VALUE rb_self, VALUE rb_x)
{
  UNWRAP( rb_self, Vector, self)
  self->x = NUM2INT( rb_x);
  return Qnil;
}


VALUE Vector_y( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( self->y);
}


VALUE Vector_y_equals( VALUE rb_self, VALUE rb_y)
{
  UNWRAP( rb_self, Vector, self)
  self->y = NUM2INT( rb_y);
  return Qnil;
}


VALUE Vector_plus( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  return new_Vector( self->x+other->x, self->y+other->y);
}


VALUE Vector_add_bang( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  self->x += other->x;
  self->y += other->y;
  return rb_self;
}


VALUE Vector_minus( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  return new_Vector( self->x-other->x, self->y-other->y);
}


VALUE Vector_subtract_bang( VALUE rb_self, VALUE rb_other)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_other, Vector, other)
  self->x -= other->x;
  self->y -= other->y;
  return rb_self;
}


VALUE Vector_multiplied_by( VALUE rb_self, VALUE rb_coefficient)
{
  UNWRAP( rb_self, Vector, self)
  Sint16  coefficient = NUM2INT( rb_coefficient);
  return new_Vector( self->x*coefficient, self->y*coefficient);
}


VALUE Vector_multiply_by_bang( VALUE rb_self, VALUE rb_coefficient)
{
  UNWRAP( rb_self, Vector, self)
  Sint16  coefficient = NUM2INT( rb_coefficient);
  self->x *= coefficient;
  self->y *= coefficient;
  return rb_self;
}


VALUE Vector_divided_by( VALUE rb_self, VALUE rb_divisor)
{
  UNWRAP( rb_self, Vector, self)
  //UNWRAP( rb_divisor, Vector, divisor)
  Sint16  divisor = NUM2INT( rb_divisor);
  return new_Vector( self->x/divisor, self->y/divisor);
}


VALUE Vector_divide_by_bang( VALUE rb_self, VALUE rb_divisor)
{
  UNWRAP( rb_self, Vector, self)
  Sint16  divisor = NUM2INT( rb_divisor);
  self->x /= divisor;
  self->y /= divisor;
  return rb_self;
}


VALUE Vector_length_squared( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( self->x*self->x + self->y*self->y);
}


VALUE Vector_length( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  return INT2FIX( sqrt((int)self->x*self->x + (int)self->y*self->y) );
}


VALUE Vector_inspect( VALUE rb_self)
{
  UNWRAP( rb_self, Vector, self)
  char  inspect[ 128];
  snprintf( inspect, sizeof(inspect), "#<SDLR::Vector:%p @x=%i @y=%i>", (void*)rb_self, self->x, self->y);
  return rb_str_new2( inspect);
}


VALUE Vector_inside( VALUE rb_self, VALUE rb_rect)
{
  UNWRAP( rb_self, Vector, self)
  UNWRAP( rb_rect, SDL_Rect, rect)
  int  outside = self->x < rect->x  || rect->x+rect->w <= self->x  ||
                 self->y < rect->y  || rect->y+rect->h <= self->y;
  return outside ? Qfalse : Qtrue;
}


void Init_Vector()
{
  rb_cVector = CLASS("Vector", rb_cObject);
  rb_define_alloc_func( rb_cVector, Vector_allocate);
  rb_define_method( rb_cVector, "initialize", Vector_initialize, 2);
  rb_define_method( rb_cVector, "x", Vector_x, 0);
  rb_define_method( rb_cVector, "x=", Vector_x_equals, 1);
  rb_define_method( rb_cVector, "y", Vector_y, 0);
  rb_define_method( rb_cVector, "y=", Vector_y_equals, 1);
  rb_define_method( rb_cVector, "+", Vector_plus, 1);
  rb_define_method( rb_cVector, "add!", Vector_add_bang, 1);
  rb_define_method( rb_cVector, "-", Vector_minus, 1);
  rb_define_method( rb_cVector, "subtract!", Vector_subtract_bang, 1);
  rb_define_method( rb_cVector, "*", Vector_multiplied_by, 1);
  rb_define_method( rb_cVector, "multiply_by!", Vector_multiply_by_bang, 1);
  rb_define_method( rb_cVector, "/", Vector_divided_by, 1);
  rb_define_method( rb_cVector, "divide_by!", Vector_divide_by_bang, 1);
  rb_define_method( rb_cVector, "length_squared", Vector_length_squared, 0);
  rb_define_method( rb_cVector, "length", Vector_length, 0);
  rb_define_method( rb_cVector, "inside", Vector_inside, 1);
  rb_define_method( rb_cVector, "inspect", Vector_inspect, 0);
  rb_define_const( rb_cVector, "ORIGIN", new_Vector(0,0) );
}


// ------------------------------------------------------------------------ Rect

VALUE Rect_allocate( VALUE class)
{
  SDL_Rect *self = ALLOC( SDL_Rect);
  return Data_Wrap_Struct( class, NULL, default_free, self);
}


VALUE Rect_initialize( VALUE rb_self, VALUE rb_top_left, VALUE rb_size)
{
  UNWRAP( rb_self,     SDL_Rect, self)
  UNWRAP( rb_top_left, Vector,   top_left)
  UNWRAP( rb_size,     Vector,   size)
  self->x = top_left->x;
  self->y = top_left->y;
  self->w = size->x;
  self->h = size->y;
  return Qnil;
}


VALUE SDLR_Rect( VALUE rb_module, VALUE rb_top_left, VALUE rb_size)
{
  VALUE  rb_self = rb_obj_alloc( rb_cRect);
  UNWRAP( rb_self,     SDL_Rect, self)
  UNWRAP( rb_top_left, Vector,   top_left)
  UNWRAP( rb_size,     Vector,   size)
  self->x = top_left->x;
  self->y = top_left->y;
  self->w = size->x;
  self->h = size->y;
  return rb_self;
}


VALUE Rect_x( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  return INT2FIX( self->x);
}


VALUE Rect_x_equals( VALUE rb_self, VALUE rb_x)
{
  UNWRAP( rb_self, SDL_Rect, self)
  self->x = NUM2INT( rb_x);
  return Qnil;
}


VALUE Rect_y( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  return INT2FIX( self->y);
}


VALUE Rect_y_equals( VALUE rb_self, VALUE rb_y)
{
  UNWRAP( rb_self, SDL_Rect, self)
  self->y = NUM2INT( rb_y);
  return Qnil;
}


VALUE Rect_w( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  return INT2FIX( self->w);
}


VALUE Rect_w_equals( VALUE rb_self, VALUE rb_w)
{
  UNWRAP( rb_self, SDL_Rect, self)
  self->w = NUM2INT( rb_w);
  return Qnil;
}


VALUE Rect_h( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  return INT2FIX( self->h);
}


VALUE Rect_h_equals( VALUE rb_self, VALUE rb_h)
{
  UNWRAP( rb_self, SDL_Rect, self)
  self->h = NUM2INT( rb_h);
  return Qnil;
}


VALUE Rect_top_left( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  return new_Vector( self->x, self->y);
}


VALUE Rect_top_left_equals( VALUE rb_self, VALUE rb_vector)
{
  UNWRAP( rb_self, SDL_Rect, self)
  UNWRAP( rb_vector, Vector, vector)
  self->x = vector->x;
  self->y = vector->y;
  return rb_self;
}


VALUE Rect_size( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  return new_Vector( self->w, self->h);
}


VALUE Rect_minus( VALUE rb_self, VALUE rb_vector)
{
  UNWRAP( rb_self, SDL_Rect, self)
  UNWRAP( rb_vector, Vector, vector)
  VALUE  rb_product = rb_obj_alloc( rb_cRect);
  UNWRAP( rb_product, SDL_Rect, product)
  product->x = self->x - vector->x;
  product->y = self->y - vector->y;
  product->w = self->w;
  product->h = self->h;
  return rb_product;
}


VALUE Rect_inspect( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Rect, self)
  char  inspect[ 128];
  snprintf( inspect, sizeof(inspect), "#<SDLR::Rect:%p @x=%i @y=%i @w=%i @h=%i>", (void*)rb_self, self->x, self->y, self->w, self->h);
  return rb_str_new2( inspect);
}


void Init_Rect()
{
  rb_cRect = CLASS("Rect", rb_cObject);
  rb_define_alloc_func( rb_cRect, Rect_allocate);
  rb_define_method( rb_cRect, "initialize", Rect_initialize, 2);
  rb_define_method( rb_cRect, "x", Rect_x, 0);
  rb_define_method( rb_cRect, "x=", Rect_x_equals, 1);
  rb_define_method( rb_cRect, "y", Rect_y, 0);
  rb_define_method( rb_cRect, "y=", Rect_y_equals, 1);
  rb_define_method( rb_cRect, "w", Rect_w, 0);
  rb_define_method( rb_cRect, "w=", Rect_w_equals, 1);
  rb_define_method( rb_cRect, "h", Rect_h, 0);
  rb_define_method( rb_cRect, "h=", Rect_h_equals, 1);
  rb_define_method( rb_cRect, "top_left", Rect_top_left, 0);
  rb_define_method( rb_cRect, "top_left=", Rect_top_left_equals, 1);
  rb_define_method( rb_cRect, "size", Rect_size, 0);
  rb_define_method( rb_cRect, "-", Rect_minus, 1);
  rb_define_method( rb_cRect, "inspect", Rect_inspect, 0);
}


// --------------------------------------------------------------------- Surface

void Surface_free( void *surf)
{
  log_gc("Surface_free %p\n", surf);
  SDL_FreeSurface( surf);
}


VALUE Surface_load( VALUE rb_class, VALUE rb_path_to_bmp)
{
  char *path_to_bmp = StringValueCStr( rb_path_to_bmp);
  SDL_Surface *self = IMG_Load( path_to_bmp);
  return Data_Wrap_Struct( rb_cSurface, NULL, Surface_free, self);
}


VALUE Surface_aura( VALUE rb_class, VALUE rb_color)
{
  Uint32  color = NUM2ULONG( rb_color);
  int  width = 64;
  int  height = 64;
  int  depth = 32;
  Uint32  rmask, gmask, bmask, amask;
/* SDL interprets each pixel as a 32-bit number, so our masks must depend
       on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif
  SDL_Surface *surf = SDL_CreateRGBSurface( SDL_HWSURFACE|SDL_SRCALPHA, width, height, depth, rmask, gmask, bmask, amask);

  if ( SDL_MUSTLOCK(surf) ) {
    if ( SDL_LockSurface(surf) )
      rb_raise( rb_eStandardError, "SDL_LockSurface returned -1");
  }

  int  radius = ( width < height ? width : height) / 2;
  Uint16  pitch = surf->pitch / 4;
  int  cx = width / 2;
  int  cy = height / 2;
  int  y, x;
  for ( y = 0;  y < surf->h;  y += 1)
  {
    int  dy = cy - y;
    Uint32 *pixel_at = &( (Uint32*) surf->pixels)[ pitch * y];
    for ( x = 0;  x < surf->w;  x += 1)
    {
      int  dx = cx - x;
      // Work out the distance ( squared) from the centre of the surface to the
      // pixel at x, y
      int  d = dx*dx + dy*dy;
      int  alpha = 255 - 256 * d / (radius*radius);
      if ( alpha < 0)
        alpha = 0;
      pixel_at[ x] = color | ( alpha << 24);
    }
  }

  if ( SDL_MUSTLOCK(surf) )
    SDL_UnlockSurface( surf);

  return Data_Wrap_Struct( rb_cSurface, NULL, Surface_free, surf);
}


VALUE Surface_width( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Surface, self)
  return INT2FIX( self->w);
}


VALUE Surface_height( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Surface, self)
  return INT2FIX( self->h);
}


VALUE Surface_display_format( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Surface, self)
  SDL_Surface *new_surf = SDL_DisplayFormat( self);
  return Data_Wrap_Struct( rb_cSurface, NULL, Surface_free, new_surf);
}


VALUE Surface_display_format_alpha( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Surface, self)
  SDL_Surface *new_surf = SDL_DisplayFormatAlpha( self);
  return Data_Wrap_Struct( rb_cSurface, NULL, Surface_free, new_surf);
}


VALUE Surface_fill_rect( VALUE rb_self, VALUE rb_area, VALUE rb_color)
{
  UNWRAP( rb_self, SDL_Surface, self)
  UNWRAP( rb_area, SDL_Rect, area)
  SDL_FillRect( self, area, NUM2ULONG(rb_color));
  return Qnil;
}


VALUE Surface_blit( VALUE rb_self, VALUE rb_from_area, VALUE rb_to)
{
  UNWRAP( rb_self, SDL_Surface, self);
  UNWRAP( rb_from_area, SDL_Rect, from_area);
  UNWRAP( rb_to, Vector, to);
  SDL_Rect  to_area = { x: to->x, y: to->y};
  SDL_BlitSurface( self, from_area, screen, &to_area);
  return Qnil;
}


VALUE Surface_blit2( VALUE rb_self, VALUE rb_from_area, VALUE rb_target, VALUE rb_to)
{
  UNWRAP( rb_self, SDL_Surface, self);
  UNWRAP( rb_from_area, SDL_Rect, from_area);
  UNWRAP( rb_target, SDL_Surface, target);
  UNWRAP( rb_to, Vector, to);
  SDL_Rect  to_area = { x: to->x, y: to->y};
  SDL_BlitSurface( self, from_area, target, &to_area);
  return Qnil;
}


VALUE Surface_blit_fill( VALUE rb_self, VALUE rb_offset, VALUE rb_to_area)
{
  UNWRAP( rb_self, SDL_Surface, self);
  UNWRAP( rb_offset, Vector, offset);
  UNWRAP( rb_to_area, SDL_Rect, to_area);
  // This surface is smaller (256x256) than the area to be filled.
  // The offset is changed between frames so it appears as if the textured
  // backdrop is scrolling through the area.

  // Clamp the offset to within the dimensions of the texture
  Sint16  ofs_x = offset->x % self->w;
  Sint16  ofs_y = offset->y % self->h;

  while( ofs_x < 0)
    ofs_x += self->w;

  while( ofs_y < 0)
    ofs_y += self->h;

  Sint16  right = to_area->x + to_area->w;
  Sint16  bottom = to_area->y + to_area->h;

  SDL_Rect  copy_area;
  SDL_Rect  top_left;

  top_left.y = to_area->y;

  while ( top_left.y < bottom)
  {
    bool first = ( top_left.y == to_area->y); // Determine if on first row or not
    copy_area.y = first ? ofs_y : 0;
    copy_area.h = first ? ( self->h - ofs_y)
                        : ( (top_left.y + self->h < bottom) ? self->h : (bottom - top_left.y) );

    top_left.x = to_area->x;

    while ( top_left.x < right)
    {
      bool first = ( top_left.x == to_area->x);
      copy_area.x = first ? ofs_x : 0;
      copy_area.w = first ? ( self->w - ofs_x)
                          : ( (top_left.x + self->w < right) ? self->w : (right - top_left.x) );
      SDL_BlitSurface( self, &copy_area, screen, &top_left);
      //printf("x:%i,y:%i,w:%i,h:%i x:%i,y:%i\n", copy_area.x, copy_area.y, copy_area.w, copy_area.h, top_left.x, top_left.y);
      top_left.x += copy_area.w;
    }

    top_left.y += copy_area.h;
  }

  return Qnil;
}


VALUE Surface_update( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Surface, self)
  SDL_Flip( self);
  return Qnil;
}


void Init_Surface()
{
  rb_cSurface = CLASS("Surface", rb_cObject);
  rb_define_singleton_method( rb_cSurface, "load", Surface_load, 1);
  rb_define_singleton_method( rb_cSurface, "aura", Surface_aura, 1);
  rb_define_method( rb_cSurface, "width", Surface_width, 0);
  rb_define_method( rb_cSurface, "height", Surface_height, 0);
  rb_define_method( rb_cSurface, "display_format", Surface_display_format, 0);
  rb_define_method( rb_cSurface, "display_format_alpha", Surface_display_format_alpha, 0);
  rb_define_method( rb_cSurface, "fill_rect", Surface_fill_rect, 2);
  rb_define_method( rb_cSurface, "blit", Surface_blit, 2);
  rb_define_method( rb_cSurface, "blit2", Surface_blit2, 3);
  rb_define_method( rb_cSurface, "blit_fill", Surface_blit_fill, 2);
  rb_define_method( rb_cSurface, "update", Surface_update, 0);
}


// ------------------------------------------------------------------------ Font

void Font_free( void *data)
{
  FontBox *self = data;
  TTF_CloseFont( self->font);
  free( self);
}


VALUE Font_allocate( VALUE class)
{
  FontBox *self = ALLOC( FontBox);
  self->font = NULL;
  return Data_Wrap_Struct( class, NULL, Font_free, self);
}


VALUE Font_initialize( VALUE rb_self, VALUE rb_path_to_font, VALUE rb_point_size)
{
  UNWRAP( rb_self, FontBox, self)
  char *path_to_font = StringValueCStr( rb_path_to_font);
  int  point_size = NUM2INT( rb_point_size);
  self->font = TTF_OpenFont( path_to_font, point_size);
  if ( NULL == self->font)
    rb_raise( rb_eStandardError, "Could not open: %s", path_to_font);
  return Qnil;
}


// NOTE: rb_color is a Fixnum such as 0x99cc33 but red is in the LEAST
// significant byte ( 0x33 here).
//
// The MS byte is either TRANSPARENT in which case Blended rendering will be
// used or OPAQUE in which case Shaded rendering will be used ( faster but
// doesn't support writing over textured backgrounds, i.e. the background color
// will be used).
//
VALUE Font_background_color_equals( VALUE rb_self, VALUE rb_color)
{
  UNWRAP( rb_self, FontBox, self)
  uint32_t *p = (uint32_t*)&self->background_color;
  *p = NUM2ULONG( rb_color);
  //printf("%i,%i,%i,%i\n", self->background_color.r, self->background_color.g, self->background_color.b, self->background_color.unused);
  return Qnil;
}


VALUE Font_text_color_equals( VALUE rb_self, VALUE rb_color)
{
  UNWRAP( rb_self, FontBox, self)
  uint32_t *p = (uint32_t*)&self->text_color;
  *p = NUM2ULONG( rb_color);
  //printf("%i,%i,%i,%i\n", self->text_color.r, self->text_color.g, self->text_color.b, self->text_color.unused);
  return Qnil;
}


VALUE Font_write( VALUE rb_self, VALUE rb_text, VALUE rb_place)
{
  UNWRAP( rb_self, FontBox, self)
  char *text = StringValueCStr( rb_text);
  UNWRAP( rb_place, Vector, place)
  SDL_Surface *surf;
  if ( TRANSPARENT == self->background_color.unused)
  {
    surf = TTF_RenderUTF8_Blended( self->font, text, self->text_color);
  }
  else {
    surf = TTF_RenderUTF8_Shaded( self->font, text, self->text_color, self->background_color);
  }
  SDL_Rect  target = { x: place->x, y: place->y };
  SDL_BlitSurface( surf, NULL, screen, &target);
  SDL_FreeSurface( surf);
  return Qnil;
}


void Init_Font()
{
  rb_cFont = CLASS("Font", rb_cObject);
  rb_define_alloc_func( rb_cFont, Font_allocate);
  rb_define_method( rb_cFont, "initialize", Font_initialize, 2);
  rb_define_method( rb_cFont, "background_color=", Font_background_color_equals, 1);
  rb_define_method( rb_cFont, "text_color=", Font_text_color_equals, 1);
  rb_define_method( rb_cFont, "write", Font_write, 2);
}


// ----------------------------------------------------------------------- Event

VALUE Event_allocate( VALUE class)
{
  SDL_Event *self = ALLOC( SDL_Event);
  return Data_Wrap_Struct( class, NULL, default_free, self);
}


VALUE Event_queue( VALUE class)
{
  SDL_Event  event;
  VALUE      rb_queue = rb_ary_new();

  while ( SDL_PollEvent(&event))
  {
    VALUE  type;
    switch ( event.type)
    {
    case SDL_KEYDOWN:         type = rb_cKeyPressedEvent; break;
    case SDL_KEYUP:           type = rb_cKeyReleasedEvent; break;
    case SDL_MOUSEMOTION:     type = rb_cMouseMotionEvent; break;
    case SDL_MOUSEBUTTONDOWN: type = rb_cMouseButtonPressedEvent; break;
    case SDL_MOUSEBUTTONUP:   type = rb_cMouseButtonReleasedEvent; break;
    default: type = rb_cEvent;
    }
    VALUE  rb_event = rb_obj_alloc( type);
    UNWRAP( rb_event, SDL_Event, ev)
    memcpy( ev, &event, sizeof(SDL_Event));
    rb_ary_push( rb_queue, rb_event);
  }

  return rb_queue;
}


VALUE Event_type( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Event, self)
  return INT2FIX( self->type);
}


VALUE Event_key( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Event, self)
  return INT2FIX( self->key.keysym.sym);
}


VALUE Event_button( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Event, self)
  switch( self->button.button) {
    case SDL_BUTTON_LEFT: return rb_sLEFT_BUTTON;
    case SDL_BUTTON_MIDDLE: return rb_sMIDDLE_BUTTON;
    case SDL_BUTTON_RIGHT: return rb_sRIGHT_BUTTON;
    default: rb_raise( rb_eStandardError, "Unexpected: %i", self->button.button);
  }
}


VALUE Event_button_where( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Event, self)
  return new_Vector( self->button.x, self->button.y);
}


// This cannot be combined reliably with Event_button_where because the x and y
// members can be at different offsets within the respective unions.
VALUE Event_motion_where( VALUE rb_self)
{
  UNWRAP( rb_self, SDL_Event, self)
  return new_Vector( self->motion.x, self->motion.y);
}


void Init_Event()
{
  rb_cEvent = CLASS("Event", rb_cObject);
  rb_cKeyboardEvent = CLASS("KeyboardEvent", rb_cEvent);
  rb_cKeyPressedEvent = CLASS("KeyPressedEvent", rb_cKeyboardEvent);
  rb_cKeyReleasedEvent = CLASS("KeyReleasedEvent", rb_cKeyboardEvent);
  rb_cMouseMotionEvent = CLASS("MouseMotionEvent", rb_cEvent);
  rb_cMouseButtonEvent = CLASS("MouseButtonEvent", rb_cEvent);
  rb_cMouseButtonPressedEvent = CLASS("MouseButtonPressedEvent", rb_cMouseButtonEvent);
  rb_cMouseButtonReleasedEvent = CLASS("MouseButtonReleasedEvent", rb_cMouseButtonEvent);

  rb_define_singleton_method( rb_cEvent, "queue", Event_queue, 0);
  rb_define_alloc_func( rb_cEvent, Event_allocate);
  rb_define_method( rb_cEvent, "type", Event_type, 0);

  rb_define_method( rb_cKeyboardEvent, "key", Event_key, 0);
  rb_define_method( rb_cMouseButtonEvent, "button", Event_button, 0);
  rb_define_method( rb_cMouseButtonEvent, "where", Event_button_where, 0);
  rb_define_method( rb_cMouseMotionEvent, "where", Event_motion_where, 0);
}


// ------------------------------------------------------------------------ SDLR

void tidy_up()
{
  printf("Terminating..\n");
  TTF_Quit();
  SDL_Quit();
}


VALUE SDLR_init( VALUE rb_module, VALUE rb_width, VALUE rb_height)
{
  if ( SDL_Init( SDL_INIT_VIDEO) < 0 ) rb_raise( rb_eStandardError, "Unable to init SDL");
  atexit( tidy_up);

  if ( TTF_Init() < 0) rb_raise( rb_eStandardError, "Unable to init SDL TTF");

  int  width = NUM2INT( rb_width);
  int  height = NUM2INT( rb_height);
  screen = SDL_SetVideoMode( width, height, 0, SDL_HWSURFACE|SDL_DOUBLEBUF);
    if ( NULL == screen) rb_raise( rb_eStandardError, "Unable to set video mode");

  SDL_ShowCursor( SDL_DISABLE);

  return Data_Wrap_Struct( rb_cSurface, NULL, Surface_free, screen);
}


VALUE SDLR_ticks( VALUE rb_module)
{
  return ULONG2NUM( SDL_GetTicks() );
}


// ------------------------------------------------------------------------ Init

typedef struct
{
  char  *name;
  short  value;
}
KeyConstant;

KeyConstant key_constants[] = {
  {"K_BACKSPACE", 8 },
  {"K_ESCAPE",   27 },
  {"K_Q",       113 },
};


void Init_SDLR()
{
  rb_mSDLR = rb_define_module("SDLR");
  rb_define_singleton_method( rb_mSDLR, "init", SDLR_init, 2);
  rb_define_module_function( rb_mSDLR, "Vector", SDLR_Vector, 2);
  rb_define_module_function( rb_mSDLR, "Rect", SDLR_Rect, 2);
  rb_define_module_function( rb_mSDLR, "ticks", SDLR_ticks, 0);

  int  i;
  for ( i = 0;  i < ARRAY_LEN( key_constants);  i += 1)
  {
    KeyConstant *kc = &key_constants[ i];
    rb_define_const( rb_mSDLR, kc->name, INT2FIX(kc->value) );
  }

  Init_Vector();
  Init_Rect();
  Init_Font();
  Init_Surface();
  Init_Event();

  rb_sLEFT_BUTTON = INT2FIX( SDL_BUTTON_LEFT);
  rb_sMIDDLE_BUTTON = INT2FIX( SDL_BUTTON_MIDDLE);
  rb_sRIGHT_BUTTON = INT2FIX( SDL_BUTTON_RIGHT);
}

// -------------------------------------------------------------- 80 column rule
