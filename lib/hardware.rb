
require "dbus"


class Hardware

  @bus = DBus::SystemBus.instance

  service = @bus.service "ro.dist.phone.Hardware"
  @remote = service.object "/Hardware"
  @remote.introspect
  @remote.default_iface = "ro.dist.phone.HardwareAPI"

  class << self

    def method_missing method_name, *params
      @remote.send( method_name, *params).first
    end

    def on_signal signal, &block
      @remote.on_signal  signal, &block
    end

    # Only required for signals.  Must be on run on the same thread on which
    # #on_signal was invoked
    #
    def run
      thread = DBus::Main.new
      thread << @bus
      thread.run
    end
  end

end

