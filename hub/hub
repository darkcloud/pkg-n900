#!/usr/bin/env ruby

$HOME = File.dirname __FILE__
look_in = lambda {|relative_path| $LOAD_PATH << File.join($HOME, relative_path) }
look_in["."]
look_in["../lib"]
look_in["../lib/SDLR"]

require "ruby_patches"
require "digest"
require "window_manager"
require "SDLR"
require "clock"
require "application"

include SDLR


class Item
  attr_reader :digest, :icon, :area, :destination

  def initialize icon
    @digest = Digest::MD5.digest( name || "")
    bytes = @digest.bytes.to_a
    @color = bytes[0] | (bytes[1] << 8) | (bytes[2] << 16)
    @icon = Surface.aura @color
    if icon
      icon.blit2 nil, @icon, Vector((64-48)/2,(64-48)/2)
    end
    @icon = @icon.display_format_alpha
    @area = Rect Vector(0, 0), Vector(64,64)
    @destination = Vector 0, 0
    @px = 0.0
    @py = 0.0
  end

  MAX_SPEED = 999 # pixels/sec
  SLOW_DOWN_AT = 99 # pixels from destination

  def update time_passed

    # Work out the vector from the current position to the destination
    to_dest = @destination - @area.top_left

    # Work out the distance to the destination
    distance = to_dest.length

    if distance != 0
      # Determine the velocity of the item towards its destination
      vx = to_dest.x.to_f / distance
      vy = to_dest.y.to_f / distance
      speed = (SLOW_DOWN_AT < distance) ? MAX_SPEED : (MAX_SPEED * distance / SLOW_DOWN_AT) # pixels/sec
      @px += vx * speed * time_passed / 1000
      @py += vy * speed * time_passed / 1000
      @area.x = @px.round
      @area.y = @py.round
    end
  end

end


class CancelItem < Item

  def initialize
    super Surface.load("debian_glow.png")
  end

  def name
    "Cancel"
  end

  def tapped
    exit
  end

end


class ApplicationItem < Item

  def initialize application
    @application = application
    super @application.icon
  end

  def name
    @application.name
  end

  def tapped
    @application.run
  end

end


class WindowItem < Item

  def initialize window
    @window = window
    super nil
  end

  def name
    @window["name"]
  end

  def tapped
    WindowManager.RaiseWindow @window["id"]
  end

end


# A ring of items.  There are multiple concentric rings.
#
class Ring

  ITEM_SPACING = 65

  SWEEP = 2 * Math::PI / 8 # An item must be within 1/8th of a circle of the ideal angle

  # Make a new ring with the specified radius in pixels from the centre.
  #
  def initialize radius
    @radius = radius
    circumference = 2 * Math::PI * radius
    capacity = ( 0 == @radius) ? 1 : ( circumference / ITEM_SPACING)
    @items = Array.new  capacity
    # Icons are equally space around the ring and therefore each item is 2 * PI
    # / count radians apart
    @arc_step = 2 * Math::PI / @items.count
  end

  # Tries to find a place for an item in this ring and adds it if there is
  # space.
  #
  #  @return  `true` if the item was added or `false` if there was no space
  #
  def add item
    # Deterministically derive the angle relative to the centre at which the
    # item should be placed from the item itself so that the angle does not
    # change over time or on different hosts yet need not be stored
    a = item.digest.bytes.first
    angle = 2*Math::PI * a / 256
    # Find the first free index ( if any) within the allowable range
    index_of = lambda {|angle| ( angle / @arc_step).to_i }
    from_i = index_of[ angle]
    to_i = index_of[ angle + SWEEP]
    to_i -= @items.count if @items.count <= to_i
    i = from_i
    loop do
      # If the index is available..
      if @items[ i].nil?
        # Plug in the item and give it the position of the slot
        @items[ i] = item
        item.destination.x = @radius * Math.sin( @arc_step * i)
        item.destination.y = @radius * Math.cos( @arc_step * i)
        return true
      end
      i = (i + 1) % @items.count
      break if i == to_i
    end
    false
  end

end


class Rings

  def initialize
    @rings = []
  end

  # Positions the items
  #
  #  @items  sorted most important first
  #
  def position items
    items.each do |item|
      ring = @rings.first_where {|ring| ring.add item }
      # If no existing ring could accomodate the item then make a new ring
      if ring.nil?
        @rings << Ring.new( radius = Ring::ITEM_SPACING*@rings.count)
        # No need to check because we *know* there's space for the item in a new ring:
        @rings.last.add item
      end
    end
  end

end


class Hub

  def initialize
    @screen = SDLR.init 800, 480

    @font = Font.new "/usr/share/fonts/truetype/droid/DroidSans-Bold.ttf", 20
    @font.text_color = 0x993333

    @backdrop = Surface.load("../backdrop.png").display_format
    @viewport = Rect Vector(-@screen.width/2,-@screen.height/2), Vector(@screen.width,@screen.height)
    @view_speed = Vector 0, 0
    @acceleration = Vector 0, 0

    @items = [ CancelItem.new ]
    WindowManager.Windows.each {|window| @items << WindowItem.new( window) if window["name"] }
    Application.all.each {|app| @items << ApplicationItem.new( app) }
    @rings = Rings.new
    @rings.position @items

    @clock = Clock.new
    @clock.target_frame_rate = 60
  end


  def main
    while true do
      events = Event.queue
      # Remove all mouse motion events except the latest
      # FIXME: Move this to C
      latest_motion_event = events.last_where {|ev| ev.is_a? MouseMotionEvent}
      events.reject! {|ev| ev.is_a? MouseMotionEvent and ev != latest_motion_event}
      events.each do |event|
        case event
        when KeyReleasedEvent
          exit if [ K_ESCAPE, K_BACKSPACE].include? event.key
          puts event.key.inspect
        when MouseMotionEvent
          if @where_pressed
            where_now = event.where
            movement = @where_pressed - where_now

            #@acceleration = movement
            #@where_pressed = where_now

            @viewport.top_left = @viewport_when_pressed + movement

            if @pressed_over_item and 8*8 < movement.length_squared
              @pressed_over_item = nil
            end
          end
        when MouseButtonPressedEvent
          @where_pressed = event.where
          @viewport_when_pressed = @viewport.top_left
          @pressed_over_item = item_at @viewport_when_pressed + @where_pressed
        when MouseButtonReleasedEvent
          where_now = event.where
          # If it was a click rather than a drag..
          if @pressed_over_item
            released_over_item = item_at  @viewport_when_pressed + where_now
            if @pressed_over_item == released_over_item
              @pressed_over_item.tapped#  where_now - item.area.top_left
            end
          end
          @where_pressed = nil
          @pressed_over_item = nil
          #@acceleration.x = @acceleration.y = 0
        else
          puts event.inspect
        end
      end

      time_passed = @clock.tick

      update  time_passed
    end
  end


  def update time_passed
    #@view_speed.add! @acceleration * time_passed / 15
    #@view_speed *= time_passed
    #@view_speed /= 30
    #@viewport.top_left += @view_speed

    @backdrop.blit_fill @viewport.top_left, Rect( Vector::ORIGIN, @viewport.size)
    @items.each do |item|
      item.update  time_passed
      area = item.area - @viewport.top_left
      item.icon.blit nil, area.top_left
    end
    @font.write "%i fps"% @clock.frame_rate, Vector(10,10)
    if @pressed_over_item
      @font.write @pressed_over_item.name, Vector(10,480-64-10-20)
    end
    @screen.update
  end


  # Provides the item at the given location on screen
  #
  def item_at place
    @items.first_where {|item| place.inside  item.area }
  end

end


hub = Hub.new
hub.main

