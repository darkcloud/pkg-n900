
class Application

  attr_reader :name, :icon

  APP_DIR = "/usr/share/applications"

  ICON_DIRS = [
    "/usr/share/pixmaps",
    "/usr/share/icons/hicolor/48x48/apps",
  ]


  class << self

    def all
      apps = []
      Dir.entries( APP_DIR).each do |entry|
        path = File.join APP_DIR, entry
        # Ignore ., .. and anything that isn't a file
        if File.file? path
          app = self.new path
          apps << app unless app.name.nil?
        end
      end
      apps
    end

  end # of class methods


  def initialize desktop_file
    File.open  desktop_file, "r:utf-8" do |file|
      file.each_line do |line|
        case line
        when /^Type=(.+)/
          @type = $1
        when /^Name=(.+)/
          @name = $1
        when /^Exec=(.+)/
          @command = $1
        when /^Icon=(.+)/
          icon_name = $1
          ICON_DIRS.each do |dir|
            path = File.join  dir, icon_name+".png"
            if File.exists?  path
              @icon = Surface.load( path)#.display_format
            end
          end
        end
      end
    end
  end

  def run
    ( pid = fork) ? Process.detach( pid) : exec(@command)
  end

end

