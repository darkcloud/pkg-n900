
# A "clock" that keeps time for framerate-independent animation.
#
class Clock

  attr_reader :frame_rate


  # Make a new Clock.
  #
  def initialize
    @when_last_ticked = ticks # so that the first invocation of tick doesn't reports that lots of time has passed
  end


  # Invoked to set the target frame rate that should ideally be achieved.
  #
  def target_frame_rate= target_frame_rate
    @target_period = 1000 / target_frame_rate # in milliseconds
  end


  # This method is invoked every time around the main loop *before* the
  # framerate-independent animation.
  #
  def tick
    # Determine the time when this method was invoked:
    now = ticks

    # Work out how much time has passed since the last invocation of this method:
    time_spent = now - @when_last_ticked

    # If less time has passed that the period of the target frame rate then the
    # system is not overloaded and it can sleep:
    if time_spent < @target_period
      sleep (@target_period - time_spent) / 1000.0
    end

    # Report the actual time that has passed since the last invocation of this
    # method for framerate-independent animation
    time_passed = ticks - @when_last_ticked

    # Remember when this method was last invoked:
    @when_last_ticked = now

    # Work out the actual frame rate:
    @frame_rate = 1000 / time_passed

    time_passed
  end

end # of Clock

